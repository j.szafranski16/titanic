import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titanic = df
    titanic['Title'] = titanic['Name'].str.extract(r' ([A-Za-z]+)\.')
    groups_by_title = titanic.groupby('Title')
    median_age_grouped_by_title = groups_by_title['Age'].median().to_dict()

    none_values_grouped_by_title = groups_by_title['Age'].apply(lambda x: x.isnull().sum()).to_dict()
    #
    considered_titles = ["Mr", "Mrs", "Miss"]

    res = [(title+'.', int(none_values_grouped_by_title.get(title, 0)), int(median_age_grouped_by_title.get(title, 0))) for title in considered_titles]
    return res
